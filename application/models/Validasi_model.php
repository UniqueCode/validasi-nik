<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Validasi_model extends CI_Model
{

    public $table = 'VALIDASI_NIK';
    public $id = 'ID';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {   
        $query = $this->db->query("SELECT a.*, TO_CHAR(CREATED_AT, 'DD-MM-YYYY HH24:MI:SS') AS TGL_INSERT FROM VALIDASI_NIK a ORDER BY ID DESC")->result();
        // $this->db->order_by($this->id, $this->order);
        // return $this->db->get($this->table)->result();
        return $query;
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL, $status = NULL, $tgl = NULL, $ket = NULL) {
        // $this->db->like('ID', $q);
        // $this->db->or_like('TRIM(NIK)', strtolower(trim($q)));
        // $this->db->or_like('LOWER(DB_ASAL)', strtolower($q));
        // $this->db->or_like('LOWER(NAMA)', strtolower($q));
        // $this->db->or_like('CREATED_AT', $q);
        $tb = $this->table;
        if($status == 'Valid'){
            $tb = 'DATA_NIK_VALID';
        }else if($status == 'Tidak Valid'){
            $tb = 'DATA_NIK_TIDAK_VALID';
        }

        if($q != NULL){
            $this->db->like('ID', $q);
            $this->db->or_like('TRIM(NIK)', strtolower(trim($q)));
            $this->db->or_like('LOWER(DB_ASAL)', strtolower($q));
            $this->db->or_like('LOWER(NAMA)', strtolower($q));
        }

        $tgl_ = date('d/m/Y');
        if($tgl != NULL){
            $tgl_ = date('d/m/Y', strtotime($tgl));
        }
        
        $this->db->where("TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$tgl_'");

        if($ket != NULL){
            $this->db->where("KETERANGAN = '$ket'");
        }

        $this->db->from($tb);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL, $status = NULL, $tgl = NULL, $ket = NULL) {
        $tb = $this->table; 
        if($status == 'Valid'){
            $tb = 'DATA_NIK_VALID';
        }else if($status == 'Tidak Valid'){
            $tb = 'DATA_NIK_TIDAK_VALID';
        }
        $this->db->select("$tb.*, TO_CHAR(CREATED_AT, 'DD-MM-YYYY HH24:MI:SS') AS TGL_INSERT");
        $this->db->order_by($this->id, $this->order);

        if($q != NULL){
            $this->db->like('ID', $q);
            $this->db->or_like('TRIM(NIK)', strtolower(trim($q)));
            $this->db->or_like('LOWER(DB_ASAL)', strtolower($q));
            $this->db->or_like('LOWER(NAMA)', strtolower($q));
            // $this->db->or_like('STATUS', $status);
            // $this->db->or_like('CREATED_AT', $q);
        }

        $tgl_ = date('d/m/Y');
        if($tgl != NULL){
            $tgl_ = date('d/m/Y', strtotime($tgl));
        }
        $this->db->where("TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$tgl_'");

        if($ket != NULL){
            $this->db->where("KETERANGAN = '$ket'");
        }

	    $this->db->limit($limit, $start);
        return $this->db->get($tb)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function total_bulanan_rows($thn = NULL, $bln = NULL) {
        $d = '01-'.$bln.'-'.$thn;
        $timestamp    = strtotime($d);
        $first_date = date('01/m/Y', $timestamp);
        $last_date  = date('t/m/Y', $timestamp);

        return $this->db->query("SELECT TO_CHAR(a.DAY, 'DD-MM-YYYY') AS TGL_INSERT,  NVL2( b.JML_DATA, b.JML_DATA, 0) JML_DATA
        FROM ( SELECT to_date('$first_date', 'DD/MM/YYYY') + ROWNUM - 1 AS DAY
        FROM ALL_OBJECTS 
        WHERE ROWNUM <= to_date('$last_date', 'DD/MM/YYYY' ) 
            - to_date('$first_date', 'DD/MM/YYYY') + 1) a LEFT JOIN (SELECT TO_DATE(CREATED_AT, 'DD/MM/YY') TGL_INSERT, COUNT(NIK) AS JML_DATA FROM VALIDASI_NIK
        WHERE TO_CHAR(CREATED_AT, 'MM') = '$bln' AND TO_CHAR(CREATED_AT, 'YYYY') = '$thn' GROUP BY TO_DATE(CREATED_AT, 'DD/MM/YY')) b ON a.DAY = b.TGL_INSERT
        ORDER BY a.DAY ASC")->count_all_results();

    }

    function get_data_bulanan($thn = NULL, $bln = NULL) {
        $d = '01-'.$bln.'-'.$thn;
        $timestamp    = strtotime($d);
        $first_date = date('01/m/Y', $timestamp);
        $last_date  = date('t/m/Y', $timestamp);

        return $this->db->query("SELECT TO_CHAR(a.DAY, 'DD-MM-YYYY') AS TGL_INSERT,  NVL2( b.JML_DATA, b.JML_DATA, 0) JML_DATA
        FROM ( SELECT to_date('$first_date', 'DD/MM/YYYY') + ROWNUM - 1 AS DAY
        FROM ALL_OBJECTS 
        WHERE ROWNUM <= to_date('$last_date', 'DD/MM/YYYY' ) 
            - to_date('$first_date', 'DD/MM/YYYY') + 1) a LEFT JOIN (SELECT TO_DATE(CREATED_AT, 'DD/MM/YY') TGL_INSERT, COUNT(NIK) AS JML_DATA FROM VALIDASI_NIK
        WHERE TO_CHAR(CREATED_AT, 'MM') = '$bln' AND TO_CHAR(CREATED_AT, 'YYYY') = '$thn' GROUP BY TO_DATE(CREATED_AT, 'DD/MM/YY')) b ON a.DAY = b.TGL_INSERT
        ORDER BY a.DAY ASC")->result();
    }
    
}

/* End of file Kamus_model.php */
/* Location: ./application/models/Kamus_model.php */