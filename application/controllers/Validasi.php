<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Validasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Validasi_model');
        $this->load->library('form_validation');
        $this->id_pengguna = get_userdata('app_id_pengguna');
    }

    public function index()
    {
        $q         = urldecode($this->input->get('q', TRUE));
        $status    = urldecode($this->input->get('status', TRUE));
        $tgl       = $this->input->get('tgl');
        $start     = intval($this->input->get('start'));
        $ket       = $this->input->get('ket');
        
        if ($q != '') 
        {
            if ($status != '') 
            {
                if ($tgl != '') 
                {
                    if($ket != '')
                    {
                        $config['base_url']  = base_url() . 'validasi?q='.urlencode($q).'&status=' . urlencode($status). '&tgl=' . $tgl .'&ket='.$ket;
                        $config['first_url'] = base_url() . 'validasi?q='.urlencode($q).'&status=' . urlencode($status). '&tgl=' . $tgl .'&ket='.$ket;
                    }
                    else
                    {
                        $config['base_url']  = base_url() . 'validasi?q='.urlencode($q).'&status=' . urlencode($status). '&tgl=' . $tgl;
                        $config['first_url'] = base_url() . 'validasi?q='.urlencode($q).'&status=' . urlencode($status). '&tgl=' . $tgl;
                    }
                }else{
                    $config['base_url']  = base_url() . 'validasi?q='.urlencode($q).'&status=' . urlencode($status);
                    $config['first_url'] = base_url() . 'validasi?q='.urlencode($q).'&status=' . urlencode($status);
                }
                
            }
            else if ($tgl != '') 
            {
                if($ket != '')
                {
                    $config['base_url']  = base_url() . 'validasi?q='.urlencode($q).'&tgl=' . $tgl.'&ket=' .$ket;
                    $config['first_url'] = base_url() . 'validasi?q='.urlencode($q).'&tgl=' . $tgl.'&ket=' .$ket;
                }
                else
                {
                    $config['base_url']  = base_url() . 'validasi?q='.urlencode($q).'&tgl=' . $tgl;
                    $config['first_url'] = base_url() . 'validasi?q='.urlencode($q).'&tgl=' . $tgl;
                }
            }
            else if ($ket != '') 
            {
                $config['base_url']  = base_url() . 'validasi?q='.urlencode($q).'&ket=' . $ket;
                $config['first_url'] = base_url() . 'validasi?q='.urlencode($q).'&ket=' . $ket;
            }
            else
            {
                $config['base_url']  = base_url() . 'validasi?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'validasi?q=' . urlencode($q);
            }
        }
        else if ($status != '') 
        {
            if ($tgl != '') 
            {
                if($ket != '')
                {
                    $config['base_url']  = base_url() . 'validasi?status=' . urlencode($status). '&tgl=' . $tgl .'&ket='.$ket;
                    $config['first_url'] = base_url() . 'validasi?status=' . urlencode($status). '&tgl=' . $tgl .'&ket='.$ket;
                }
                else
                {
                    $config['base_url']  = base_url() . 'validasi?status='.urlencode($status).'&tgl=' . $tgl;
                    $config['first_url'] = base_url() . 'validasi?status='.urlencode($status).'&tgl=' . $tgl;
                }
            }
            else if($ket != '')
            {
                $config['base_url']  = base_url() . 'validasi?status=' . urlencode($status). '&ket='.$ket;
                $config['first_url'] = base_url() . 'validasi?status=' . urlencode($status). '&ket='.$ket;
            }
            else
            {
                $config['base_url']  = base_url() . 'validasi?status=' . urlencode($status);
                $config['first_url'] = base_url() . 'validasi?status=' . urlencode($status);
            }
            // echo var_dump($config);die();
        }
        else if ($tgl != '') 
        {
            if($ket != '')
            {
                $config['base_url']  = base_url() . 'validasi?tgl=' . $tgl .'&ket='.$ket;
                $config['first_url'] = base_url() . 'validasi?tgl=' . $tgl .'&ket='.$ket;
            }
            else
            {
                $config['base_url']  = base_url() . 'validasi?tgl=' . $tgl;
                $config['first_url'] = base_url() . 'validasi?tgl=' . $tgl;
            }
        }
        else if ($ket != '') 
        {
            
            $config['base_url']  = base_url() . 'validasi?ket=' . $ket;
            $config['first_url'] = base_url() . 'validasi?ket=' . $ket;
        }
        else 
        {
            $config['base_url']  = base_url() . 'validasi';
            $config['first_url'] = base_url() . 'validasi';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Validasi_model->total_rows($q, $status, $tgl, $ket);
        $validasi                    = $this->Validasi_model->get_limit_data($config['per_page'], $start, $q, $status, $tgl, $ket);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'validasi_data' => $validasi,
            'q'             => $q,
            'status'        => $status,
            'tgl'           => $tgl,
            'ket'           => $ket,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Rekap Harian',
            'create'        => 'validasi/create',
            'validasi'      => site_url('validasi/proses'),
        );
        
        $this->template->load('layout', 'validasi/validasi_list', $data);
    }   

    public function proses()
    {
        $nik = $_POST['nik'];
        $nama = $_POST['nama'];
        $db = $_POST['db_asal'];
        $v_db = "V_DATA_NIK_".$db;
        
        $cek = $this->db->query("SELECT * FROM VALIDASI_NIK WHERE NIK='$nik' AND DB_ASAL='$db'")->row();
        if(!empty($cek)){
            set_flashdata('warning', 'Data sudah pernah divalidasi.');
            redirect(site_url('validasi'));
        }else{
            $row = $this->db->query("SELECT * FROM $v_db WHERE TRIM(NIK)='$nik'")->row();
            // echo var_dump($row);die();
            if(empty($row)){
                set_flashdata('warning', 'Data NIK tidak ditemukan pada database '. $db);
                redirect(site_url('validasi'));
            }else{
                $nik = $row->NIK;
                $nama = $row->NAMA;
    
                $curl = curl_init();
                curl_setopt_array($curl, [
                    CURLOPT_URL => "http://192.168.1.210/validasinik.php?nik=".$nik."&nama=".urlencode($nama),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_TIMEOUT => 60,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => [
                        "Content-Type: application/json"
                    ],
                ]);
        
                $response = curl_exec($curl);
                $err = curl_error($curl);
        
                curl_close($curl);
                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $obj = json_decode($response, true);
                    $pesan = "Tidak Valid";
    
                    if(isset($obj['content'][0]['NIK'])){
                        $pesan = "Valid";
                    }
                    
                    $data['NIK'] = $nik;
                    $data['NAMA'] = $nama;
                    $data['STATUS'] = $pesan;
                    if($db=='BPHTB'){
                        $data['DB_ASAL'] = 'BPHTB';
                        $data['BPHTB_SSPD_ID'] = $row->BPHTB_SSPD_ID;
                        $data['DATA_ID'] = $row->ID;
                    }else if($db=='PBB'){
                        $data['DB_ASAL'] = 'PBB';
                        $jalan = "";
                        if($row->JALAN_WP != ""){
                            $jalan = $row->JALAN_WP;
                        }
                        $blok="";
                        if($row->BLOK_KAV_NO_WP != ""){
                            $blok = ', Blok : '.$row->BLOK_KAV_NO_WP;
                        }
                        $rt="";
                        if($row->RT_WP != ""){
                            $rt = ', RT/RW : '.$row->RT_WP.'/';
                        }
                        $rw="";
                        if($row->RW_WP != ""){
                            $rw = $rt.$row->RW_WP;
                        }
                        $kel="";
                        if($row->KELURAHAN_WP != ""){
                            $kel = ', Kel. '.$row->KELURAHAN_WP;
                        }
                        $kota="";
                        if($row->KOTA_WP != ""){
                            $kota = ', '.$row->KOTA_WP;
                        }
                        $kode_pos="";
                        if($row->KD_POS_WP != ""){
                            $kode_pos = ', '.$row->KD_POS_WP;
                        }
                        $data['ALAMAT'] = $jalan.$blok.$rw.$kel.$kota.$kode_pos;
                    }else if($db=='PDRD'){
                        $data['DB_ASAL'] = 'PDRD';
                        $data['ALAMAT'] = $row->ALAMAT;
                    }
                    
                    $this->db->set('CREATED_AT',"TO_DATE(SYSDATE,'dd/mm/yyyy hh24:mi:ss')",false);
                    $insert = $this->db->insert('VALIDASI_NIK', $data);
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $this->db->trans_commit();
                        set_flashdata('success', 'Data telah divalidasi.');
                        redirect(site_url('validasi'));
                    }
                }
            }
			
        }
        
    }

    public function ajax_nik(){
        $db = $_GET['db'];
        $nik = $_GET['nik'];
        $nama = "kosong";
        if($db=="BPHTB"){
            $cek_db = $this->db->query("SELECT * FROM V_DATA_NIK_BPHTB WHERE TRIM(NIK)='$nik'")->row();
            if(!empty($cek_db)){
                $nama = $cek_db->NAMA;
            }
        }else if($db=="PBB"){
            $cek_db = $this->db->query("SELECT * FROM V_DATA_NIK_PBB WHERE TRIM(NIK)='$nik'")->row();
            if(!empty($cek_db)){
                $nama = $cek_db->NAMA;
            }
        }else if($db=="PDRD"){
            $cek_db = $this->db->query("SELECT * FROM V_DATA_NIK_PDRD WHERE TRIM(NIK)='$nik'")->row();
            if(!empty($cek_db)){
                $nama = $cek_db->NAMA;
            }
        }

        echo $nama;
    }

    public function cek()
    {
        
        ini_set('max_execution_time', '1200');
        $nik  = $_POST['nik'];
        $nama = $_POST['nama'];
        $pesan = "";
        $pesan_teks = "";
        if((strlen($nik) == 16) && (substr($nik,0,1) != '0') && (is_numeric($nik) == true)){
			$curl = curl_init();
			curl_setopt_array($curl, [
				CURLOPT_URL => "http://192.168.1.125/dukcapil/validasinik.php?nik=".$nik."&nama=".urlencode($nama),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => [
					"Content-Type: application/json"
				],
			]);
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
	
			$obj  = json_decode($response, true);

            if(isset($obj['content'][0]['NIK']))
            {
                if($obj['content'][0]['NIK'] == "Sesuai")
                {
                    $pesan_teks = "Data NIK Ditemukan";
                    
                    set_flashdata('success', $pesan_teks);
                }
            } 
            else 
            {
                if(($obj['content'][0]['RESPONSE_CODE']=="15") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Tidak Ditemukan, NIK tidak sesuai format Dukcapil')
                    || ($obj['content'][0]['RESPONSE_CODE']=="13") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Tidak Ditemukan, NIK tidak terdapat di database Kependudukan'))
                {
                    if($obj['content'][0]['RESPONSE_CODE']=="15"){
                        $pesan_teks = "Data Tidak Ditemukan, NIK tidak sesuai format Dukcapil";
                        set_flashdata('error', $pesan_teks);
                    }else if($obj['content'][0]['RESPONSE_CODE']=="13"){
                        $pesan_teks = "Data Tidak Ditemukan, NIK tidak terdapat di database Kependudukan";
                        set_flashdata('error', $pesan_teks);
                    }
                }
                else if(($obj['content'][0]['RESPONSE_CODE']=="11") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Ditemukan, Meninggal Dunia')){
                    $pesan_teks = "Data Ditemukan, Meninggal Dunia";
                    set_flashdata('success', $pesan_teks);
                }
                else if(($obj['content'][0]['RESPONSE_CODE']=="12") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Ditemukan, Data Ganda')){
                    $pesan_teks = "Data Ditemukan, Data Ganda";
                    set_flashdata('success', $pesan_teks);
                }
                else if(($obj['content'][0]['RESPONSE_CODE']=="05") || ($obj['content'][0]['RESPONSE_DESC'] == 'Kuota Akses Hari Ini Telah Habis')){
                    $pesan_teks = "Kuota Akses Hari Ini Telah Habis";
                    set_flashdata('error', $pesan_teks);
                }
            }
            
		}else{
            set_flashdata('error', 'Format NIK tidak valid!');
		}
        
        redirect(site_url('validasi'));
        // if((strlen($nik) == 16) && (substr($nik,0,1) != '0') && (is_numeric($nik) == true)){
        //     $curl = curl_init();
        //     curl_setopt_array($curl, [
        //         CURLOPT_URL => "http://192.168.1.125/dukcapil/validasinik.php?nik=".$nik."&nama=".urlencode($nama),
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_TIMEOUT => 60,
        //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //         CURLOPT_CUSTOMREQUEST => "GET",
        //         CURLOPT_HTTPHEADER => [
        //             "Content-Type: application/json"
        //         ],
        //     ]);
            
        //     $response = curl_exec($curl);
        //     $err = curl_error($curl);
            
        //     curl_close($curl);
        //     if ($err) {
        //         echo "cURL Error #:" . $err;
        //     } else {
        //         $obj = json_decode($response, true);
        //         $pesan = "";
        
        //         if(isset($obj['content'][0]['NIK'])){
        //             $pesan = "NIK ".$nik." terdeteksi valid !";
        //             set_flashdata('success', $pesan);
        //         }
    
        //         if(isset($obj['content'][0]['RESPONSE_DESC'])){
        //             $pesan = $obj['content'][0]['RESPONSE_DESC'];
        //             set_flashdata('error', $pesan);
        //         }
    
        //         redirect(site_url('validasi'));
        //     }
        // }else{
        //     set_flashdata('error', 'Format NIK tidak valid !');
        //     redirect(site_url('validasi'));
        // }
        
        
    }

    public function bulanan()
    {
        $tahun    = $this->input->get('tahun');
        $bulan    = $this->input->get('bulan');
        $start  = intval($this->input->get('start'));
        
        $config['base_url']  = base_url() . 'validasi/bulanan?tahun='.$tahun.'&bulan=' . $bulan;
        $config['first_url'] = base_url() . 'validasi/bulanan?tahun='.$tahun.'&bulan=' . $bulan;

        
        $validasi   = $this->Validasi_model->get_data_bulanan($tahun, $bulan);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'validasi_data' => $validasi,
            'tahun'         => $tahun,
            'bulan'         => $bulan,
            'start'         => $start,
            'title'         => 'Rekap Bulanan',
        );
        $this->template->load('layout', 'validasi/validasi_bulanan_list', $data);
    }   
    
    // public function _rules()
    // {
    //     $this->form_validation->set_rules('english', 'english', 'trim|required');
    //     $this->form_validation->set_rules('indonesia', 'indonesia', 'trim|required');
    //     $this->form_validation->set_rules('kategori_eng', 'kategori eng', 'trim|required');
    //     $this->form_validation->set_rules('kategori_ina', 'kategori ina', 'trim|required');
    //     // $this->form_validation->set_rules('date_insert', 'date insert', 'trim|required');
    //     // $this->form_validation->set_rules('date_update', 'date update', 'trim|required');

    //     $this->form_validation->set_rules('id', 'id', 'trim');
    //     $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    // }
}

/* End of file Kamus.php */
/* Location: ./application/controllers/Kamus.php */
