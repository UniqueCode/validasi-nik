<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->nama=get_userdata('app_nama');
    }
	public function index()
	{
		
		$now   = date('d/m/Y');
		$row   = $this->db->query("SELECT COUNT(*) JML_DATA FROM VALIDASI_NIK WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$now'")->row();
		$pbb   = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM V_DATA_NIK_PBB WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = 'PBB')")->row()->JML_DATA;
		$pdrd  = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM V_DATA_NIK_PDRD WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = 'PDRD')")->row()->JML_DATA;
		$bphtb = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM V_DATA_NIK_BPHTB WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = 'BPHTB')")->row()->JML_DATA;
		$data=array(
			'title'        => 'Dashboard',
			'validasi_now' => $row->JML_DATA,
			'tgl_now'      => date('d-m-Y'),
			'pbb'          => $pbb,
			'pdrd'         => $pdrd,
			'bphtb'        => $bphtb
		);
		$this->template->load('layout','dashboard', $data);
		// $this->load->view('layout2',$data);
		// echo base_url();
	}
}
