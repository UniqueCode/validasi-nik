<?php
/**
 * 
 */
class CekNik extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this->nama_db = 'BPHTB';
    }

	function index()
	{
		echo "Start validasi". PHP_EOL;
		//set jenis db
		$db = $this->nama_db;
		ini_set('max_execution_time', '0');
		try 
		{
			$this->db->trans_begin();

			$keterangan = "";
			$pesan_teks = "";
			
			$v_db = "V_DATA_NIK_".$db;

			$tot_nik_pbb = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM V_DATA_NIK_PBB WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = 'PBB')")->row();

			if($tot_nik_pbb->JML_DATA > 0)
			{
				$v_db = "V_DATA_NIK_PBB";
				$db = "PBB";
			}else
			{
				$tot_nik_bphtb = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM V_DATA_NIK_BPHTB WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = 'BPHTB')")->row();
				
				if($tot_nik_bphtb->JML_DATA > 0)
				{
					$v_db = "V_DATA_NIK_BPHTB";
					$db = "BPHTB";
				}else
				{
					$tot_nik_pdrd = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM V_DATA_NIK_PDRD WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = 'PDRD')")->row();
					
					if($tot_nik_pdrd->JML_DATA > 0)
					{
						$v_db = "V_DATA_NIK_PDRD";
						$db = "PDRD";
					}
				}
			}


			$cek_data = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM $v_db WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = '$db')")->row();

			if($cek_data->JML_DATA > 0)
			{
				// echo $cek_data->JML_DATA. PHP_EOLs;
				// exit();
				$cek_now = $this->db->query("SELECT * FROM VALIDASI_NIK WHERE TO_DATE(CREATED_AT, 'DD/MM/YYYY') = TO_DATE(SYSDATE, 'DD/MM/YYYY') AND KETERANGAN = 1")->result();
			
				if(count($cek_now) < 20000 )
				{
					for ($i=1; $i <= 500; $i++) 
					{ 					
						$cek_now = $this->db->query("SELECT * FROM VALIDASI_NIK WHERE TO_DATE(CREATED_AT, 'DD/MM/YYYY') = TO_DATE(SYSDATE, 'DD/MM/YYYY') AND KETERANGAN = 1")->result();
						
						if(count($cek_now) < 20000 )
						{
							$row  = $this->db->query("SELECT * FROM $v_db WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = '$db') AND ROWNUM <= 1")->row();
							
							if(!empty($row)){
								$nik  = preg_replace('/\s+/', '', $row->NIK);
								$nama1 = str_replace("/","",$row->NAMA);
								$nama2 = stripslashes($nama1);
								$nama3 = str_replace('"','`',$nama2);
								$nama4 = str_replace("'",'`',$nama3);
								$nama = $nama4;
		
								echo $i.'. '.$nik.'-'.$nama. PHP_EOL;
		
								$pesan        = "Tidak Valid";
								$data['NIK']  = $nik;
								$data['NAMA'] = $nama;
		
								if($db=='BPHTB')
								{
									$data['DB_ASAL']       = 'BPHTB';
									// $data['BPHTB_SSPD_ID'] = $row->BPHTB_SSPD_ID;
									// $data['DATA_ID']       = $row->ID;
								}
								else if($db=='PBB')
								{
									$data['DB_ASAL'] = 'PBB';
		
									$jalan = "";
									if($row->JALAN_WP != "")
									{
										$jalan = $row->JALAN_WP;
									}
		
									$blok="";
									if($row->BLOK_KAV_NO_WP != "")
									{
										$blok = ', Blok : '.$row->BLOK_KAV_NO_WP;
									}
		
									$rt="";
									if($row->RT_WP != "")
									{
										$rt = ', RT/RW : '.$row->RT_WP.'/';
									}
		
									$rw="";
									if($row->RW_WP != "")
									{
										$rw = $rt.$row->RW_WP;
									}
		
									$kel="";
									if($row->KELURAHAN_WP != "")
									{
										$kel = ', Kel. '.$row->KELURAHAN_WP;
									}
		
									$kota="";
									if($row->KOTA_WP != "")
									{
										$kota = ', '.$row->KOTA_WP;
									}
		
									$kode_pos="";
									if($row->KD_POS_WP != "")
									{
										$kode_pos = ', '.$row->KD_POS_WP;
									}
		
									$data['ALAMAT'] = $jalan.$blok.$rw.$kel.$kota.$kode_pos;
								}
								else if($db=='PDRD')
								{
									$data['DB_ASAL'] = 'PDRD';
									$data['ALAMAT']  = $row->ALAMAT;
								}
		
								if((strlen($nik) == 16) && (substr($nik,0,1) != '0') && (is_numeric($nik) == true))
								{
									//cek total data ketika looping
									$data_now = $this->db->query("SELECT * FROM VALIDASI_NIK 
									WHERE TO_DATE(CREATED_AT, 'DD/MM/YYYY') = TO_DATE(SYSDATE, 'DD/MM/YYYY') AND KETERANGAN = 1")->result();
									if(count($data_now) < 20000)
									{
										$curl = curl_init();
										
										curl_setopt_array($curl, [
											CURLOPT_URL => "http://192.168.1.125/dukcapil/validasinik.php?nik=".$nik."&nama=".urlencode($nama),
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_TIMEOUT => 60,
											CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
											CURLOPT_CUSTOMREQUEST => "GET",
											CURLOPT_HTTPHEADER => [
												"Content-Type: application/json"
											],
										]);
									
										$response = curl_exec($curl);
										$err      = curl_error($curl);
						
										curl_close($curl);
						
										if ($err) 
										{
											echo "cURL Error #:" . $err;
										} 
										else 
										{
											$obj    = json_decode($response, true);
											$insert = "yes";
		
											if(isset($obj['content'][0]['NIK']))
											{
												if($obj['content'][0]['NIK'] == "Sesuai")
												{
													$pesan = "Valid";
													$pesan_teks = "Data Ditemukan";
												}
											} 
											else 
											{
												if(isset($obj['content'][0]['RESPONSE_DESC']))
												{
													if(($obj['content'][0]['RESPONSE_CODE']=="15") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Tidak Ditemukan, NIK tidak sesuai format Dukcapil')
													|| ($obj['content'][0]['RESPONSE_CODE']=="13") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Tidak Ditemukan, NIK tidak terdapat di database Kependudukan'))
													{
														$pesan  = "Tidak Valid";
														$insert = "yes";
														if($obj['content'][0]['RESPONSE_CODE']=="15"){
															$pesan_teks = "Data Tidak Ditemukan, NIK tidak sesuai format Dukcapil";
														}else if($obj['content'][0]['RESPONSE_CODE']=="13"){
															$pesan_teks = "Data Tidak Ditemukan, NIK tidak terdapat di database Kependudukan";
														}
													}
													else if(($obj['content'][0]['RESPONSE_CODE']=="11") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Ditemukan, Meninggal Dunia')){
														$pesan  = "Valid";
														$insert = "yes";
														$pesan_teks = "Data Ditemukan, Meninggal Dunia";
													}
													else if(($obj['content'][0]['RESPONSE_CODE']=="12") || ($obj['content'][0]['RESPONSE_DESC'] == 'Data Ditemukan, Data Ganda')){
														$pesan  = "Valid";
														$insert = "yes";
														$pesan_teks = "Data Ditemukan, Data Ganda";
													}
													else if(($obj['content'][0]['RESPONSE_CODE']=="05") || ($obj['content'][0]['RESPONSE_DESC'] == 'Kuota Akses Hari Ini Telah Habis')){
														echo "Kuota Akses Hari Ini Telah Habis". PHP_EOL;
														break;
														$insert = "no";
													}
													else
													{
														$insert = "no";
													}
												}
												else
												{
													$insert = "no";
												}
											}
		
											// if(isset($obj['content'][0]['RESPONSE_CODE'])){
											// 	if($obj['content'][0]['RESPONSE_CODE']!='15'){
											// 		$insert="no";
											// 	}
											// }
		
											if($insert=="yes")
											{
												$data['STATUS'] = $pesan;
												$data['KETERANGAN'] = '1';
												$data['PESAN'] = $pesan_teks;
												$tgl = date('d/m/Y H:i:s');
												$this->db->set('CREATED_AT',"TO_DATE('$tgl','DD/MM/YYYY HH24:MI:SS')",false);
												$this->db->insert('VALIDASI_NIK', $data);
												if ($this->db->trans_status() === FALSE) 
												{
													$this->db->trans_rollback();
												} 
												else 
												{
													$this->db->trans_commit();
												}
											}
										}
									}
									else 
									{
										echo "Kuota Habis". PHP_EOL;
									}
								}else{
									// $pesan = "Tidak Valid";
									// $data['STATUS'] = $pesan;
									// $data['KETERANGAN'] = '0';
									// $data['PESAN'] = "Data Tidak Ditemukan, NIK tidak sesuai format Dukcapil";
									// $tgl = date('d/m/Y H:i:s');
									// $this->db->set('CREATED_AT',"TO_DATE('$tgl','DD/MM/YYYY HH24:MI:SS')",false);
									// $this->db->insert('VALIDASI_NIK', $data);
									// if ($this->db->trans_status() === FALSE)
									// {
									// 	$this->db->trans_rollback();
									// } 
									// else 
									// {
									// 	$this->db->trans_commit();
									// }
								}
							}else{
								$keterangan = $i." data berhasil di validasi";
								echo $keterangan. PHP_EOL;
								break;
							}
						}
						
	
						if($i==500)
						{
							$keterangan = "500 data berhasil di validasi";
							echo $keterangan. PHP_EOL;
						}
						
					}
				}else{
					echo '20.000 data telah selesai divalidasi'. PHP_EOL;
				}
			}
			else
			{
				echo count($cek_data).' data '.$db.' telah selesai divalidasi'. PHP_EOL;
			}
		} 
		catch (Exception $e) 
		{
			echo $e->getMessage();
		}		
	}

	public function testing()
	{
		echo "Start validasi". PHP_EOL;
		//set jenis db
		$db = "PBB";
		ini_set('max_execution_time', '1200');
		try 
		{
			$this->db->trans_begin();

			$keterangan = "";
			
			$v_db = "V_DATA_NIK_".$db;

			$cek_data = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM $v_db WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = '$db')")->row();
			
			if($cek_data->JML_DATA > 0)
			{
				// echo $cek_data->JML_DATA. PHP_EOL;
				// exit();
				$res  = $this->db->query("SELECT * FROM $v_db WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = '$db') AND ROWNUM <= 100")->result();
				$i = 1;
				foreach ($res as $row) {
					$cek_now = $this->db->query("SELECT * FROM VALIDASI_NIK WHERE TO_DATE(CREATED_AT, 'DD/MM/YYYY') = TO_DATE(SYSDATE, 'DD/MM/YYYY') AND KETERANGAN = 1")->result();
					
					if(count($cek_now) < 980 )
					{
						// $row  = $this->db->query("SELECT * FROM $v_db WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = '$db') AND ROWNUM <= 1")->row();
						$nik  = preg_replace('/\s+/', '', $row->NIK);
						$nama = $row->NAMA;

						echo $i.'. '.$nik.'-'.$nama. PHP_EOL;

						$pesan        = "Tidak Valid";
						$data['NIK']  = $nik;
						$data['NAMA'] = $nama;

						if($db=='BPHTB')
						{
							$data['DB_ASAL']       = 'BPHTB';
							$data['BPHTB_SSPD_ID'] = $row->BPHTB_SSPD_ID;
							$data['DATA_ID']       = $row->ID;
						}
						else if($db=='PBB')
						{
							$data['DB_ASAL'] = 'PBB';

							$jalan = "";
							if($row->JALAN_WP != "")
							{
								$jalan = $row->JALAN_WP;
							}

							$blok="";
							if($row->BLOK_KAV_NO_WP != "")
							{
								$blok = ', Blok : '.$row->BLOK_KAV_NO_WP;
							}

							$rt="";
							if($row->RT_WP != "")
							{
								$rt = ', RT/RW : '.$row->RT_WP.'/';
							}

							$rw="";
							if($row->RW_WP != "")
							{
								$rw = $rt.$row->RW_WP;
							}

							$kel="";
							if($row->KELURAHAN_WP != "")
							{
								$kel = ', Kel. '.$row->KELURAHAN_WP;
							}

							$kota="";
							if($row->KOTA_WP != "")
							{
								$kota = ', '.$row->KOTA_WP;
							}

							$kode_pos="";
							if($row->KD_POS_WP != "")
							{
								$kode_pos = ', '.$row->KD_POS_WP;
							}

							$data['ALAMAT'] = $jalan.$blok.$rw.$kel.$kota.$kode_pos;
						}
						else if($db=='PDRD')
						{
							$data['DB_ASAL'] = 'PDRD';
							$data['ALAMAT']  = $row->ALAMAT;
						}

						if((strlen($nik) == 16) && (substr($nik,0,1) != '0') && (is_numeric($nik) == true))
						{
							//cek total data ketika looping
							$data_now = $this->db->query("SELECT * FROM VALIDASI_NIK 
							WHERE TO_DATE(CREATED_AT, 'DD/MM/YYYY') = TO_DATE(SYSDATE, 'DD/MM/YYYY') AND KETERANGAN = 1")->result();
							if(count($data_now) < 980)
							{
								echo $pesan. PHP_EOL;
							}
							else 
							{
								echo "Kuota Habis". PHP_EOL;
							}
						}else{
							echo "tidak valid". PHP_EOL;
						}
					}
					

					if($i==100)
					{
						$keterangan = "100 data berhasil di validasi";
						echo $keterangan. PHP_EOL;
					}

					$i++;
				}
			}
			else
			{
				echo count($cek_data).' data '.$db.' telah selesai divalidasi'. PHP_EOL;
			}
		} 
		catch (Exception $e) 
		{
			echo $e->getMessage();
		}		
	}

	public function cek(){
		
		ini_set('max_execution_time', '1200');
		$nik=$_GET['nik'];
		$nama= urldecode(stripslashes($_GET['nama']));
		if((strlen($nik) == 16) && (substr($nik,0,1) != '0') && (is_numeric($nik) == true)){
			$curl = curl_init();
			curl_setopt_array($curl, [
				CURLOPT_URL => "http://192.168.1.125/dukcapil/validasinik.php?nik=".$nik."&nama=".urlencode($nama),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => [
					"Content-Type: application/json"
				],
			]);
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
	
			print_r($response);
			echo "<pre>";
			echo date('d/m/Y H:i:s');
		}else{
			echo "NIK tidak valid";
		}
	}

	public function hari_ini(){
		try {
			$now       = date('d/m/Y');
			$yesterday = date('d/m/Y', strtotime("-1 day"));
			// echo $yesterday;
			// die();
			$db          = $this->nama_db;
			$cek         = $this->db->query("SELECT COUNT(*) JML_DATA FROM VALIDASI_NIK WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$now'")->row();
			$valid       = $this->db->query("SELECT COUNT(*) JML_DATA FROM DATA_NIK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$now'")->row();
			$tidak_valid = $this->db->query("SELECT COUNT(*) JML_DATA FROM DATA_NIK_TIDAK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$now'")->row();

			$cek_kemarin 		 = $this->db->query("SELECT COUNT(*) JML_DATA FROM VALIDASI_NIK WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$yesterday'")->row();
			$valid_kemarin       = $this->db->query("SELECT COUNT(*) JML_DATA FROM DATA_NIK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$yesterday'")->row();
			$tidak_valid_kemarin = $this->db->query("SELECT COUNT(*) JML_DATA FROM DATA_NIK_TIDAK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY') = '$yesterday'")->row();

			$v_db        = "V_DATA_NIK_".$db;
			$cek_data    = $this->db->query("SELECT COUNT(*) AS JML_DATA FROM $v_db WHERE TRIM(NIK) NOT IN (SELECT TRIM(NIK) FROM VALIDASI_NIK WHERE DB_ASAL = '$db')")->row();
			// echo var_dump(number_format($cek_data->JML_DATA,0,",","."));
			// die();
			

			//cek nik yang tervalidasi kemarin
			$curl = curl_init();
			$token = "Bearer hMpqnLAJMq662UtfG1XtgjNKmM9YWdrSRrjvy1iJbuu8pRos9p";
			$data = array(
				'list_phone' => '6285706019703,6282141360125',
				// 'list_phone' => '6285706019703,',
				'message' => "Total NIK yang tervalidasi kemarin _(".$yesterday.")_ : *".$cek_kemarin->JML_DATA."* (Valid : *".$valid_kemarin->JML_DATA."*, Tidak Valid : *".$tidak_valid_kemarin->JML_DATA."*)",
			);
			curl_setopt($curl, CURLOPT_HTTPHEADER,
				array(
					"Authorization: $token",
					"Content-Type: application/json"
				)
			);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST , true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_URL,  "192.168.1.205/ncm/api/send_message");
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

			$result = curl_exec($curl);
			curl_close($curl);
			echo $result. PHP_EOL;

			//cek nik yang tervalidasi hari ini
			$curl = curl_init();
			$token = "Bearer hMpqnLAJMq662UtfG1XtgjNKmM9YWdrSRrjvy1iJbuu8pRos9p";
			$data = array(
				'list_phone' => '6285706019703,6282141360125',
				// 'list_phone' => '6285706019703,',
				'message' => "Total NIK yang tervalidasi hari ini _(".$now.")_ : *".$cek->JML_DATA."* (Valid : *".$valid->JML_DATA."*, Tidak Valid : *".$tidak_valid->JML_DATA."*)",
			);
			curl_setopt($curl, CURLOPT_HTTPHEADER,
				array(
					"Authorization: $token",
					"Content-Type: application/json"
				)
			);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST , true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_URL,  "192.168.1.205/ncm/api/send_message");
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

			$result = curl_exec($curl);
			curl_close($curl);
			echo $result. PHP_EOL;

			//cek nik yang belum tervalidasi
			$curl = curl_init();
			$token = "Bearer hMpqnLAJMq662UtfG1XtgjNKmM9YWdrSRrjvy1iJbuu8pRos9p";
			$data = array(
				'list_phone' => '6285706019703',
				'message' => "Total NIK yang belum tervalidasi dari database *".$db."* per hari ini _(".$now.")_ : *".number_format($cek_data->JML_DATA,0,",",".")."*",
			);
			curl_setopt($curl, CURLOPT_HTTPHEADER,
				array(
					"Authorization: $token",
					"Content-Type: application/json"
				)
			);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST , true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_URL,  "192.168.1.205/ncm/api/send_message_validasi_nik");
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

			$result = curl_exec($curl);
			curl_close($curl);
			echo $result. PHP_EOL;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function cek_vpn(){
		ini_set('max_execution_time', '1200');
		$nik = "3507130808800002";
		$nama= "AGUS JUNAIDI";

		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => "http://192.168.1.125/dukcapil/validasinik.php?nik=".$nik."&nama=".urlencode($nama),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [
				"Content-Type: application/json"
			],
		]);
		
		$response = curl_exec($curl);
		$obj      = json_decode($response, true);
		curl_close($curl);

		if(isset($obj['content'][0]['NIK']))
		{
			echo "terhubung";
		}else{
			$ch = curl_init();
			$token = "Bearer hMpqnLAJMq662UtfG1XtgjNKmM9YWdrSRrjvy1iJbuu8pRos9p";
			$data = array(
				'list_phone' => '6285706019703,6282141360125',
				'message' => "*VPN Dukcapil terputus. Silahkan dicek !*",
			);
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array(
					"Authorization: $token",
					"Content-Type: application/json"
				)
			);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST , true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_URL,  "192.168.1.205/ncm/api/send_message");
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$result = curl_exec($ch);
			curl_close($ch);
			echo $result. PHP_EOL;
		}
		
	}
}
?>