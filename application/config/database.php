<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

// $tnsname = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.1.205)(PORT = 1521))
// 		(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = duakosong5)))';

$tnsname = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = oracle19c)(PORT = 1521))
		(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = opdb205)))';

$db['default'] = array(
	'dsn'          => '',
	'hostname'     => $tnsname,
	'username'     => 'KEPENDUDUKAN',
	'password'     => 'KEPENDUDUKAN321',
	'database'     => '',
	'dbdriver'     => 'oci8',
	'dbprefix'     => '',
	'pconnect'     => FALSE,
	'db_debug'     => (ENVIRONMENT !== 'production'),
	'cache_on'     => FALSE,
	'cachedir'     => '',
	'char_set'     => 'utf8',
	'dbcollat'     => 'utf8_general_ci',
	'swap_pre'     => '',
	'encrypt'      => FALSE,
	'compress'     => FALSE,
	'stricton'     => FALSE,
	'failover'     => array(),
	'save_queries' => TRUE
);

