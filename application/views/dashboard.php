<div class="row">
    <div class="col-md-4">
      <a href="<?= base_url('validasi?q=&status=&tgl=').$tgl_now?>">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="ion-ios-person"></i></span>
        <div class="info-box-content">
          <span class="info-box-text" tyle="font-size: 12pt">Validasi Hari Ini (<b><?= date('d-m-Y')?></b>)</span>
          <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="info-box-number" style="font-size: 25pt; text-align: center"><?php echo number_format("$validasi_now",0,",",".");?></span>
        </div>
      </div>
      </a>
    </div>
    <div class="col-md-4">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="ion-ios-person"></i></span>
        <div class="info-box-content">
          <span class="info-box-text" tyle="font-size: 12pt">NIK Belum Tervalidasi Dari PBB</span>
          <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="info-box-number" style="font-size: 25pt; text-align: center"><?php echo number_format("$pbb",0,",",".");?></span>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="info-box bg-maroon">
        <span class="info-box-icon"><i class="ion-ios-person"></i></span>
        <div class="info-box-content">
          <span class="info-box-text" tyle="font-size: 12pt">NIK Belum Tervalidasi Dari PDRD</span>
          <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="info-box-number" style="font-size: 25pt; text-align: center"><?php echo number_format("$pdrd",0,",",".");?></span>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="ion-ios-person"></i></span>
        <div class="info-box-content">
          <span class="info-box-text" tyle="font-size: 12pt">NIK Belum Tervalidasi Dari BPHTB</span>
          <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="info-box-number" style="font-size: 25pt; text-align: center"><?php echo number_format("$bphtb",0,",",".");?></span>
        </div>
      </div>
    </div>
</div>