<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">

            <form role="form" action="<?= $action ?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                    <label for="nama_menu">Nama Menu <sup style="color:red;">*</sup></label>
                    <input type="text" class="form-control" value="<?= $nama_menu?>" name="nama_menu" id="nama_menu" required>
                    <?php echo form_error('nama_menu') ?>
                </div>
                <div class="form-group">
                    <label for="link_menu">Link Menu <sup style="color:red;">*</sup></label>
                    <input type="text" class="form-control" value="<?= $link_menu?>" name="link_menu" id="link_menu" required>
                    <?php echo form_error('link_menu') ?>
                </div>
                <div class="form-group">
                    <label>Parent <sup style="color:red;">*</sup></label>
                    <select name="parent" id="parent" class="form-control">
                        <option value="0">Is parent</option>
                        <?php foreach($parent_menu as $pm){?>
                            <option <?php if($pm->id_inc==$parent){echo "selected";} ?> value="<?= $pm->id_inc?>"><?= $pm->nama_menu?></option>
                        <?php } ?>
                    </select>
                    <?php echo form_error('parent') ?>
                </div>
                <div class="form-group">
                    <label for="sort">Sort <sup style="color:red;">*</sup></label>
                    <input type="text" class="form-control" value="<?= $sort?>" name="sort" id="sort" required>
                    <?php echo form_error('sort') ?>
                </div>
                <div class="form-group">
                    <label for="icon">Icon Menu <sup style="color:red;">*</sup></label>
                    <input type="text" class="form-control" value="<?= $icon?>" name="icon" id="icon" required>
                    <?php echo form_error('icon') ?>
                </div>
              </div>
              <!-- /.box-body -->

              <input type="hidden" name="id_inc" value="<?= $id_inc ?>">
              <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                <?= anchor('menu','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
              </div>
            </form>
            </div>
        </div>
    </div>
</div>