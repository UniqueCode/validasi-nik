<div class="row">
<div class="col-md-12">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body">
    <div class="row">
          <form action="<?php echo site_url('validasi'); ?>"  method="get">
              <div class="col-md-3">
                  <div class="form-group">
                      <label>Pencarian</label>
                      <input type="text" class="form-control"  placeholder="Pencarian" name="q" value="<?= $q?>">
                  </div>
              </div>
              <div class="col-md-3" style="padding-top:25px">
                  <div class="form-group">
                      <label ></label>
                      <?php  if ($q <> '') {   ?>
                        <a href="<?php echo site_url('validasi'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                      <?php   } ?>

                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                  </div>
              </div>
          </form>
      </div>
      <table class="table table-bordered">
      <thead>
          <tr>
            <th style="text-align: center">No</th>
            <th style="text-align: center">NIK</th>
            <th style="text-align: center">NAMA</th>
            <th style="text-align: center">ALAMAT</th>
            <th style="text-align: center">DB ASAL</th>
            <th style="text-align: center">STATUS</th>
            <th style="text-align: center" width="13%" >TANGGAL INSERT</th>
          </tr>
        </thead>
        <tbody><?php
        foreach ($validasi_data as $a)
        {
            ?>
            <tr>
			<td align="center" width="80px"><?php echo ++$start ?></td>
			<td><?php echo $a->NIK ?></td>
			<td><?php echo $a->NAMA ?></td>
			<td><?php echo $a->ALAMAT ?></td>
			<td><?php echo $a->DB_ASAL ?></td>
			<td style="text-align: center"><?php if($a->STATUS=='Valid'){ ?><span class="label label-success"><?= $a->STATUS ?></span><?php }else{ ?><span class="label label-danger"><?= $a->STATUS ?></span><?php } ?></td>
			<td style="text-align: center"><?php echo $a->TGL_INSERT ?></td>
		</tr>
                  <?php
              }
              ?>
              </tbody>
      </table>
    </div>
    <div class="box-footer clearfix">
      <span class="pull-left">
      <button type="button" class="btn btn-block btn-success btn-sm">Record : <?php echo $total_rows ?></button>
      </span>
            <?php echo $pagination ?>
      </div>
  </div>
</div>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Validasi NIK</h4>
      </div>
      <form action="<?= base_url("validasi/cek") ?>" method="post" role="form">
      <div class="modal-body">
        <div class="box-body">
          <!-- <div class="form-group">
            <label for="nama">Database Asal  <sup style="color:red;">*</sup></label>
            <select class="form-control" style="width: 100%;" name="db_asal" id="db_asal" required>
              <option value="">Pilih</option>
              <option value="BPHTB">BPHTB</option>
              <option value="PBB">PBB</option>
              <option value="PDRD">PDRD</option>
            </select>
          </div> -->
          <!-- <div class="form-group">
            <label for="nik">NIK  <sup style="color:red;">*</sup></label>
            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" maxlength="16" class="form-control" name="nik" id="nik" placeholder="Masukkan NIK" required>
          </div> -->
          <div class="form-group">
            <label for="nik">NIK  <sup style="color:red;">*</sup></label>
            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" maxlength="16" class="form-control" name="nik" id="nik" placeholder="Masukkan NIK" required>
          </div>
          <div class="form-group">
            <label for="nama">Nama Lengkap </label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Lengkap">
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-simpan">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
  // $("#nik").keyup(function(){
  //   console.log($('#nik').val());
  //   if($('#nik').val().length == 16){
  //     $.get("<?= base_url('validasi/ajax_nik')?>", {db: $('#db_asal').val(), nik : $('#nik').val()} , function(data){
            
  //           console.log(data);
  //       if(data=="kosong"){
  //         $('#nama').val('');
  //       }else{
  //         $('#nama').val(data);

  //       }
  //     });
  //   }
  // });
</script>