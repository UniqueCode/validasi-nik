<div class="row">
<div class="col-md-9">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body">
    <div class="row">
          <form action="<?php echo site_url('validasi/bulanan'); ?>"  method="get">
              <div class="col-md-3">
                  <div class="form-group">
                      <label>Tahun</label>
                      <select class="form-control" style="width: 100%;" name="tahun" id="tahun">
                        <option value="">Pilih</option>
                        <?php
                        for ($i=2022; $i < (2022+5); $i++) { 
                        ?>
                        <option <?php if($tahun==$i){ echo "selected"; } ?> value="<?= $i ?>"><?= $i ?></option>
                        <?php
                        }
                        ?>
                      </select>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label>Bulan</label>
                      <select class="form-control" style="width: 100%;" name="bulan" id="bulan">
                        <option value="">Pilih</option>
                        <option <?php if($bulan=='01'){ echo "selected"; } ?> value="01">Januari</option>
                        <option <?php if($bulan=='02'){ echo "selected"; } ?> value="02">Februari</option>
                        <option <?php if($bulan=='03'){ echo "selected"; } ?> value="03">Maret</option>
                        <option <?php if($bulan=='04'){ echo "selected"; } ?> value="04">April</option>
                        <option <?php if($bulan=='05'){ echo "selected"; } ?> value="05">Mei</option>
                        <option <?php if($bulan=='06'){ echo "selected"; } ?> value="06">Juni</option>
                        <option <?php if($bulan=='07'){ echo "selected"; } ?> value="07">Juli</option>
                        <option <?php if($bulan=='08'){ echo "selected"; } ?> value="08">Agustus</option>
                        <option <?php if($bulan=='09'){ echo "selected"; } ?> value="09">September</option>
                        <option <?php if($bulan=='10'){ echo "selected"; } ?> value="10">Oktober</option>
                        <option <?php if($bulan=='11'){ echo "selected"; } ?> value="11">November</option>
                        <option <?php if($bulan=='12'){ echo "selected"; } ?> value="12">Desember</option>
                      </select>
                  </div>
              </div>
              <div class="col-md-4" style="padding-top:25px">
                  <div class="form-group">
                      <label ></label>
                      <?php  if ($tahun <> '' || $bulan <> '') {   ?>
                        <a href="<?php echo site_url('validasi/bulanan?tahun=').date('Y').'&bulan='.date('m'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                      <?php   } ?>

                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                  </div>
              </div>
          </form>
      </div>
      <div class="col-md-12">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width="2%" rowspan="2" style="vertical-align : middle;text-align:center;">NO</th>
            <th rowspan="2" style="vertical-align : middle;text-align:center;">TANGGAL</th>
            <th style="text-align: center" width="20%" colspan="2">SERVICE CAPIL</th>
            <!-- <th width="20%" rowspan="2" style="vertical-align : middle;text-align:center;">TIDAK VALID <br>(<i>non service capil</i>)</th> -->
            <th rowspan="2" style="vertical-align : middle;text-align:center;">TOTAL VALIDASI</th>
            <!-- <th style="text-align: center">AKSI</th> -->
          </tr>
          <tr>
            <th style="text-align: center" width="20%">VALID</th>
            <th style="text-align: center" width="20%">TIDAK VALID</th>
            <!-- <th style="text-align: center">AKSI</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
            $start = 0;
            foreach ($validasi_data as $a)
            {
              $tgl = date('d/m/Y', strtotime($a->TGL_INSERT));
              $valid = $this->db->query("SELECT COUNT(*) JML FROM DATA_NIK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY')='$tgl'")->row();
              $tidak_valid = $this->db->query("SELECT COUNT(*) JML FROM DATA_NIK_TIDAK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY')='$tgl' AND KETERANGAN=1")->row();
              $tidak_valid2 = $this->db->query("SELECT COUNT(*) JML FROM DATA_NIK_TIDAK_VALID WHERE TO_CHAR(CREATED_AT, 'DD/MM/YYYY')='$tgl' AND KETERANGAN=0")->row();
                ?>
                <tr>
                  <td align="center" width="80px"><?php echo ++$start ?></td>
                  <td align="center"><?php echo $a->TGL_INSERT ?></td>
                  <td align="right">
                    <?php 
                      if($valid->JML==0){
                        echo $valid->JML;
                      }else{
                    ?>
                      <a href="<?= base_url('validasi?q=&status=Valid&tgl=').$a->TGL_INSERT.'&ket=1'; ?>" style="font-weight: 1000; font-size: 13pt"><small class="label pull-right bg-green"><?= $valid->JML ?></small></a>
                    <?php
                      }
                    ?>
                  </td>
                  <td align="right">
                    <?php 
                      if($tidak_valid->JML==0){
                        echo $tidak_valid->JML;
                      }else{
                    ?>
                      <a href="<?= base_url('validasi?q=&status='.urlencode('Tidak Valid').'&tgl=').$a->TGL_INSERT.'&ket=1'; ?>" style="font-weight: 1000; font-size: 13pt"><small class="label pull-right bg-red"><?= $tidak_valid->JML ?></small></a>
                    <?php
                      }
                    ?>
                  </td>
                  <!-- <td align="right">
                    <?php 
                      if($tidak_valid2->JML==0){
                        echo $tidak_valid2->JML;
                      }else{
                    ?>
                      <a href="<?= base_url('validasi?q=&status='.urlencode('Tidak Valid').'&tgl=').$a->TGL_INSERT.'&ket=0'; ?>" style="font-weight: 1000; font-size: 13pt"><small class="label pull-right bg-yellow"><?= $tidak_valid2->JML ?></small></a>
                    <?php
                      }
                    ?>
                  </td> -->
                  <td align="right">
                    <?php 
                      if($a->JML_DATA==0){
                        echo $a->JML_DATA;
                      }else{
                    ?>
                      <a href="<?= base_url('validasi?q=&status=&tgl=').$a->TGL_INSERT; ?>" style="font-weight: 1000; font-size: 13pt"><small class="label pull-right bg-blue"><?= $a->JML_DATA ?></small></a>
                    <?php
                      }
                    ?>
                  </td>
                  <!-- <td align="center">
                    <?php if($a->JML_DATA==0){
                    ?>
                      <button class="btn btn-sm btn-success" disabled>Detail</button>
                    <?php
                    }else{
                    ?>
                      <a href="<?= base_url('validasi?q=&status=&tgl=').$a->TGL_INSERT; ?>" class="btn btn-sm btn-success">Detail</a>
                    <?php
                    }
                    ?>
                  </td> -->
                </tr>
          <?php
            }
          ?>
        </tbody>
        <!-- <thead>
          <tr>
            <th width="2%" rowspan="2" style="vertical-align : middle;text-align:center;">NO</th>
            <th rowspan="2" style="vertical-align : middle;text-align:center;">TANGGAL</th>
            <th style="text-align: center" width="20%" colspan="2">SERVICE CAPIL</th>
            <th width="20%" rowspan="2" style="vertical-align : middle;text-align:center;">TIDAK VALID <br>(<i>non service capil</i>)</th>
            <th rowspan="2" style="vertical-align : middle;text-align:center;">TOTAL VALIDASI</th>
            
          </tr>
          <tr>
            <th style="text-align: center" width="20%">VALID</th>
            <th style="text-align: center" width="20%">TIDAK VALID</th>
          </tr>
        </thead> -->
      </table>
      </div>
      
    </div>
  </div>
</div>
</div>


