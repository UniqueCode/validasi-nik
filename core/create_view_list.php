<?php
$string="<div class=\"row\">
<div class=\"col-md-12\">
  <div class=\"box\">
    <!-- /.box-header -->
    <div class=\"box-body\">
    <div class=\"row\">
          <form action=\"<?php echo site_url('$c_url'); ?>\"  method=\"get\">
              <div class=\"col-md-3\">
                  <div class=\"form-group\">
                      <label>Pencarian</label>
                      <input type=\"text\" class=\"form-control\"  placeholder=\"Pencarian\" name=\"q\" value=\"<?= \$q?>\">
                  </div>
              </div>
              <div class=\"col-md-3\" style=\"padding-top:25px\">
                  <div class=\"form-group\">
                      <label ></label>
                      <?php  if (\$q <> '') {   ?>
                        <a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-warning\"><i class=\"fa fa-refresh\"></i> Reset</a>
                      <?php   } ?>

                      <button class=\"btn btn-primary\" type=\"submit\"><i class=\"fa fa-search\"></i> Cari</button>
                  </div>
              </div>
          </form>
      </div>
      <table class=\"table table-bordered\">
      <thead>
          <tr>
              <th>No</th>";
              foreach ($non_pk as $row) {
                  $string .= "\n\t\t<th>" . label($row['column_name']) . "</th>";
              }
              $string .= "\n\t\t<th></th>
          </tr>
        </thead>
        <tbody>";

        $string .= "<?php
        foreach ($" . $c_url . "_data as \$$c_url)
        {
            ?>
            <tr>";
            $string .= "\n\t\t\t<td align=\"center\" width=\"80px\"><?php echo ++\$start ?></td>";
            foreach ($non_pk as $row) {
              $string .= "\n\t\t\t<td><?php echo $" . $c_url ."->". $row['column_name'] . " ?></td>";
          }
          $string .= "\n\t\t\t<td style=\"text-align:center\" width=\"100px\">"
          . "\n\t\t\t\t<?php if(\$akses['is_update']==1){"
          . "\n\t\t\t\techo anchor(site_url('".$c_url."/update/'.acak($".$c_url."->".$pk.")),'<i class=\"fa fa-edit\"></i>','class=\"btn btn-sm btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Edit data\"'); }"
          . "\n\t\t\t\t if(\$akses['is_delete']==1){echo anchor(site_url('".$c_url."/delete/'.acak($".$c_url."->".$pk.")),'<i class=\"fa fa-trash\"></i>','class=\"btn btn-sm btn-danger btn-xs\" onclick=\"javasciprt: return confirm(\\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Hapus data\"'); }"
          . "\n\t\t\t\t?>"
          . "\n\t\t\t</td>";

  $string .=  "\n\t\t</tr>
                  <?php
              }
              ?>
              </tbody>
      </table>
    </div>
    <div class=\"box-footer clearfix\">
      <span class=\"pull-left\">
      <button type=\"button\" class=\"btn btn-block btn-success btn-sm\">Record : <?php echo \$total_rows ?></button>
      </span>
            <?php echo \$pagination ?>
      </div>
  </div>
</div>
</div>";
/*$string="<div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card-box\">
                    <div class=\"row\">
                        <div class=\"col-md-12 \">
                             <form action=\"<?php echo site_url('$c_url'); ?>\" class=\"form-horizontal\" method=\"get\">
                                <div class=\"form-group row\">
                                    <label class=\"col-1 col-form-label\">Pencarian</label>
                                    <div class=\"col-4\">
                                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"q\" value=\"<?php echo \$q; ?>\">
                                    </div>
                                    <div class=\"col-4\">
                                        <?php  if (\$q <> '') {   ?>
                                                <a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-warning\"><i class=\"fa fa-refresh\"></i> Reset</a>
                                        <?php   } ?>
                                        <button class=\"btn btn-primary\" type=\"submit\"><i class=\"fa fa-search\"></i> Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=\"table-responsive\">
                        <table class=\"table table-bordered\" style=\"margin-bottom: 10px\">
                            <thead class=\"thead-light\">
                            <tr>
                                <th>No</th>";
                                    foreach ($non_pk as $row) {
                                        $string .= "\n\t\t<th>" . label($row['column_name']) . "</th>";
                                    }
                                    $string .= "\n\t\t<th></th>
                                </tr>
                                </thead>
                            <tbody>
                            ";
                $string .= "<?php
                            foreach ($" . $c_url . "_data as \$$c_url)
                            {
                                ?>
                                <tr>";

                $string .= "\n\t\t\t<td align=\"center\" width=\"80px\"><?php echo ++\$start ?></td>";
                foreach ($non_pk as $row) {
                    $string .= "\n\t\t\t<td><?php echo $" . $c_url ."->". $row['column_name'] . " ?></td>";
                }


                $string .= "\n\t\t\t<td style=\"text-align:center\" width=\"100px\">"
                        . "\n\t\t\t\t<?php if(\$akses['is_update']==1){"
                        . "\n\t\t\t\techo anchor(site_url('".$c_url."/update/'.acak($".$c_url."->".$pk.")),'<i class=\"fa fa-edit\"></i>','class=\"badge badge-info\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Edit data\"'); }"
                        . "\n\t\t\t\t if(\$akses['is_delete']==1){echo anchor(site_url('".$c_url."/delete/'.acak($".$c_url."->".$pk.")),'<i class=\"fa fa-trash\"></i>','class=\"badge badge-danger\" onclick=\"javasciprt: return confirm(\\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Hapus data\"'); }"
                        . "\n\t\t\t\t?>"
                        . "\n\t\t\t</td>";

                $string .=  "\n\t\t</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            <a href=\"#\" class=\"btn btn-success\">Total Data : <?php echo \$total_rows ?></a>";

            $string .= "\n\t</div>
                        <div class=\"col-md-6 text-right\">
                            <?php echo \$pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>";*/
/*$string = "
        <div class=\"row\" >

            <div class=\"col-md-4\">
                            <?php echo anchor(site_url('".$c_url."/create'),'Create', 'class=\"btn btn-primary\"'); ?>
                        </div>
            <div class=\"col-md-4 text-center\">
                <div style=\"margin-top: 8px\" id=\"message\">
                    <?php echo \$this->session->userdata('message') <> '' ? \$this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class=\"col-md-1 text-right\">
            </div>

        </div>

        <div class=\"row\">
            <div class=\"col-md-6\">
                <a href=\"#\" class=\"btn btn-primary\">Total Record : <?php echo \$total_rows ?></a>";
if ($export_excel == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/excel'), 'Excel', 'class=\"btn btn-primary\"'); ?>";
}
if ($export_word == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/word'), 'Word', 'class=\"btn btn-primary\"'); ?>";
}
if ($export_pdf == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/pdf'), 'PDF', 'class=\"btn btn-primary\"'); ?>";
}
$string .= "\n\t    </div>
            <div class=\"col-md-6 text-right\">
                <?php echo \$pagination ?>
            </div>
        </div>
    ";
*/



$hasil_view_list = createFile($string, $target."views/" . $c_url . "/" . $v_list_file);

?>